# Material Icons

A demo project on how to use material icons in your web projects

## Installation
### Setup Method 1. Using via Google Fonts
```html
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
```

### Setup Method 2. Self hosting
https://developers.google.com/fonts/docs/material_icons#setup_method_2_self_hosting

## Get the icon codes
https://fonts.google.com/icons?icon.set=Material+Icons&icon.style=Filled

